import React from 'react';
import ContainerHeader from 'components/ContainerHeader/index';
import IntlMessages from 'util/IntlMessages';
import SessCustomerTable from '../../../components/SessCustomersTable/';

class SessCustomers extends React.Component {

  render() {
    return (
      <div className="app-wrapper">
        <ContainerHeader match={this.props.match} title={<IntlMessages id="pages.sessCustomersTitle"/>}/>
          <div className="animated">
              <div className="row">
                  <div className="col-12">
                      <div className="row">
                          <div className="col-12">
                              <div className="jr-card">
                                  <div className="jr-card-header d-flex align-items-center">
                                      <h3 className="mb-0"><IntlMessages id="table.sessCustomersTableOneTitle"/></h3>
                                  </div>
                                  <SessCustomerTable table={'t1'}/>
                              </div>
                          </div>
                      </div>
                      <div className="row">
                          <div className="col-12">
                              <div className="jr-card">
                                  <div className="jr-card-header d-flex align-items-center">
                                      <h3 className="mb-0"><IntlMessages id="table.sessCustomersTableTwoTitle"/></h3>
                                  </div>
                                  <SessCustomerTable table={'t2'}/>
                              </div>
                          </div>
                      </div>
                      <div className="row">
                          <div className="col-12">
                              <div className="jr-card">
                                  <div className="jr-card-header d-flex align-items-center">
                                      <h3 className="mb-0"><IntlMessages id="table.sessCustomersTableThreeTitle"/></h3>
                                  </div>
                                  <SessCustomerTable table={'t3'}/>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="col-12"></div>
              </div>
          </div>

      </div>
    );
  }
}

export default SessCustomers;