import React from 'react';
import Button from '@material-ui/core/Button';


class SessCustomersTableCell extends React.Component {

    constructor() {
        super();
    }

    render() {
        const {id, status, cif, customerName} = this.props.data;
        const statusStyle = status.includes("OK") ? "text-white bg-success" : status.includes("WARNING") ? "bg-amber" : status.includes("DANGER") ? "text-white bg-danger" : "text-white bg-grey";
        return (
            <tr
                tabIndex={-1}
                key={id}
            >
                <td className="text-center">
                    <div className={` badge text-uppercase ${statusStyle}`} style={{width:'20px',height:'20px',display:'block',borderRadius:'50%'}}>{}</div>
                </td>
                <td>{cif}</td>
                <td>{customerName}</td>
                <td style={{padding:'15px 10px'}}>
                    <Button variant="contained" className="jr-btn jr-btn-sm bg-warning text-white">Assess</Button>
                    <Button variant="contained" className="jr-btn jr-btn-sm bg-info text-white">Remove</Button>
                </td>
            </tr>

        );
    }
}

export default SessCustomersTableCell;
