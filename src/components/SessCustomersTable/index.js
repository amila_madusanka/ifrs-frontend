import React, {Component} from 'react';
import SessCustomersTableCell from './SessCustomersTableCell';


let counter = 0;

function createData(status, cif, customerName) {
    counter += 1;
    return {id: counter, status, cif, customerName};
}

class SessCustomerTable extends Component {
    state = {
        data: {
            t1: [
                createData('DANGER', '675897345690', 'SUNKEM CONSTRUCTIONS (PVT) LTD'),
                createData('DANGER', '998100717705', 'ST CLAIR TEA CEYLON LTD')
            ],
            t2: [
                createData('OK', '675897345690', 'MAHAWELI MEGA DEVELOPMENTS'),
                createData('WARNING', '998100717705', 'M/S AITKEN SPENCE TRAVELS')
            ],
            t3: [
                createData('OK', '675897345690', 'MAJSTRO SOLUTIONS (PVT) LTD'),
                createData('DANGER', '998100717705', 'CEYLON PHARMACEUTICLS'),
                createData('OK', '998100717705', 'NAWALOKA MEDICAL (PVT) LTD'),
                createData('OK', '998100717705', 'SOUTHERN DEVELOPMENT PROJECT')
            ]
        }
    };

    render() {
        const {data} = this.state;
        return (
            <div className="table-responsive-material">
                <table className="default-table table-unbordered table table-sm table-hover">
                    <thead className="th-border-b">
                    <tr>
                        <th></th>
                        <th>CIF #</th>
                        <th>Customer Name</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data[this.props.table].map(data => {
                        return (
                            <SessCustomersTableCell key={data.id} data={data}/>
                        );
                    })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default SessCustomerTable;